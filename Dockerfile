FROM node:9

RUN mkdir /opt/julie
WORKDIR /opt/julie
COPY . /opt/julie

# RUN npm rebuild

CMD ["node", "dist/server", "--use-openssl-ca"]

EXPOSE 4000
