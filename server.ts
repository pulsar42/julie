import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import { enableProdMode } from '@angular/core';

// Express Engine
import { ngExpressEngine } from '@nguniversal/express-engine';
// Import module map for lazy loading
import { provideModuleMap } from '@nguniversal/module-map-ngfactory-loader';

import * as express from 'express';
import * as request from "request-promise-native";
import { join } from 'path';
const { environment } = require('./server/main');
import { Response } from 'express';

// Faster server renders w/ Prod mode (dev mode never needed)
enableProdMode();

// Express server
const app = express();

const PORT = process.env.PORT || 4000;
const DIST_FOLDER = join(process.cwd(), 'dist');

// * NOTE :: leave this as require() since this file is built Dynamically from webpack
const { AppServerModuleNgFactory, LAZY_MODULE_MAP } = require('./server/main');

// Our Universal express-engine (found @ https://github.com/angular/universal/tree/master/modules/express-engine)
app.engine('html', ngExpressEngine({
    bootstrap: AppServerModuleNgFactory,
    providers: [
        provideModuleMap(LAZY_MODULE_MAP),
        // In case you want to use an AppShell with SSR and Lazy loading
        // you'd need to uncomment the below. (see: https://github.com/angular/angular-cli/issues/9202)
        // {
        //   provide: NgModuleFactoryLoader,
        //   useClass: ModuleMapNgFactoryLoader,
        //   deps: [
        //     Compiler,
        //     MODULE_MAP
        //   ],
        // },
    ]
}));

app.set('view engine', 'html');
app.set('views', join(DIST_FOLDER, 'browser'));

// Example Express Rest API endpoints
// app.get('/api/**', (req, res) => { });

const uglyRssFix = (body:string) => body.replace(/https?:\/\/wp\.pulsar42\.sc/g, 'https://pulsar42.sc')

app.get('/feed', async (req, res: Response) => {
    try {
        const feed = await request.get(environment.wordpress.url + "?feed=rss2", {}, (error, response, body) => {
            if (error) {
                console.error("error accesssing rss", error);
                res.sendStatus(500);
            } else {
                res
                    .set({
                        "Content-Type": "application/rss+xml"
                    });
                res.send(uglyRssFix(body));
            }
        });
    } catch (e) {
        res.sendStatus(500);
    }
});

app.get('/rss', async (req, res: Response) => {
    try {
        const feed = await request.get(environment.wordpress.url + "?feed=rss", {}, (error, response, body) => {
            if (error) {
                console.error("error accesssing rss", error);
                res.sendStatus(500);
            } else {
                res
                    .set({
                        "Content-Type": "application/rss+xml"
                    });
                res.send(uglyRssFix(body));
            }
        });
    } catch (e) {
        res.sendStatus(500);
    }
});

app.get('/rss2', async (req, res: Response) => {
    try {
        const feed = await request.get(environment.wordpress.url + "?feed=rss2", {}, (error, response, body) => {

            if (error) {
                console.error("error accesssing rss2", error);
                res.sendStatus(500);
            } else {
                res
                .set({
                    "Content-Type": "application/rss+xml"
                });
                res.send(uglyRssFix(body));
            }


        });
    } catch (e) {
        res.sendStatus(500);
    }
});



// Robots
app.get('robots.txt', (req, res) => {
    res.send('User-Agent: *\nAllow: / ');
});

// Server static files from /browser
app.get('*.*', express.static(join(DIST_FOLDER, 'browser'), {
    maxAge: '1y'
}));

// All regular routes use the Universal engine
app.get('*', (req, res) => {
    res.render('index', { req });
});

// Start up the Node server
app.listen(PORT, () => {
    console.log(`Node Express server listening on http://localhost:${PORT}`);
});
