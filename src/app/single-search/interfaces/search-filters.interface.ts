export interface SearchFilters {
    search: string;
    types?: SearchOnType[];
    categories?: number[];
}

export enum SearchOnType {
    TRADUCTION = "traduction",
    POST = "post"
}