/**
 * Update some or all filters
 */
export class SetFilters {
  static readonly type = '[Search] Set Filters';
  constructor(public payload: string) { }
}

/**
 * Fetch results for our current filters
 */
export class FetchResults {
    static readonly type = '[Search] Fetch results'
}


/**
 * Toggle the showing of filters in the search page
 */
export class ToggleShowFilters {
    static readonly type = '[Search] Toggle show filters'
}