import { WPApiService } from './../../core/services/wp-api.service';
import { State, Action, StateContext } from '@ngxs/store';
import { FetchResults, ToggleShowFilters } from './search.actions';
import { SearchFilters } from '../interfaces/search-filters.interface';
import { APISingleArticle } from '../../single/interfaces/articles.interface';
import { map } from 'rxjs/operators';

export class SearchStateModel {
  public items: APISingleArticle[];
  public filters: { model: SearchFilters };
  public fetching: boolean;
  public displayFilters: boolean;
}

@State<SearchStateModel>({
  name: 'search',
  defaults: {
    items: [],
    displayFilters: false,
    fetching: false,
    filters: {
        model: {
            search: ""
        }
    },
  }
})
export class SearchState {

    constructor(
        protected _wpApi: WPApiService
    ) {

    }

    @Action(FetchResults)
    public fetchResults(ctx: StateContext<SearchStateModel>) {
        if(ctx.getState().fetching) return;

        const filters = ctx.getState().filters;

        const search = filters.model.search;
        if (!search || search.length < 3) return ctx.patchState({ items: [], fetching: false });
        
        const args: Record<string, any> = {
            search,
            "type[]": ['post', 'traduction']
        }

        if (filters.model.categories) {
            args["categories[]"] = filters.model.categories;
        }
        

        ctx.patchState({ fetching: true })
        return this._wpApi.get<APISingleArticle[]>(`wp/v2/multiple-post-type`, args).pipe(
            map((articles) => {
                return ctx.patchState({ items: articles, fetching: false })
            }));
    }

    @Action(ToggleShowFilters)
    public toggleShowFilters(ctx: StateContext<SearchStateModel>) {
        return ctx.patchState({ displayFilters: !ctx.getState().displayFilters })
    }

}
