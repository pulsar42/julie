import { FetchResults, ToggleShowFilters } from './../../state/search.actions';
import { FeaturedState, RootState } from './../../../core/state/index';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SearchStateModel } from '../../state/search.state';
import { WPCategory } from '../../../core/interfaces/wp-category.interface';

@Component({
    selector: 'pulsar-search-page',
    templateUrl: './search-page.component.html',
    styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {

    @Select((state: FeaturedState) => state.search)
    public state$: Observable<SearchStateModel>


    @Select((state: FeaturedState) => state.search.filters.model)
    public form$: Observable<SearchStateModel>

    @Select((state: RootState) => state.core.categories)
    public categories$: Observable<WPCategory[]>

    public form: FormGroup = this._formBuilder.group({
        search: new FormControl('', [Validators.required, Validators.minLength(3)]),
        categories: '',
    })

    constructor(
        protected _formBuilder: FormBuilder,
        protected _store: Store
    ) { 
        this.form$.subscribe(() => {
            this._store.dispatch(new FetchResults())
        })
    }

    ngOnInit() {
    }

    public resetForm() {
        setTimeout(() => {
            this.form.reset()
            this.form.updateValueAndValidity()
        })
    }

    public toggleFilters() {
        this._store.dispatch(new ToggleShowFilters())
    }

}
