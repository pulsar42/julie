import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { NgxsModule } from '@ngxs/store';
import { FlexLayoutModule } from '@angular/flex-layout';
import { RouterModule } from '@angular/router';
import { SearchPageComponent } from './components/search-page/search-page.component';
import { MatInputModule, MatIconModule, MatProgressSpinnerModule, MatToolbarModule, MatSelectModule } from '@angular/material';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { SearchState } from './state/search.state';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SearchPageComponent],
  imports: [
    CommonModule,
    CoreModule,
    MatIconModule,    
    NgxsFormPluginModule,
    NgxsModule.forFeature([SearchState]),
    FlexLayoutModule,
    MatInputModule,
    MatToolbarModule,
    MatSelectModule,
    MatProgressSpinnerModule,
    ReactiveFormsModule,
    RouterModule.forChild([
        { path: '', component: SearchPageComponent, pathMatch: 'full' }
    ]),
  ]
})
export class SingleSearchModule { }
