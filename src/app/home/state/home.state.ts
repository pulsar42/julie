import { WPApiService } from './../../core/services/wp-api.service';
import { APIHome } from './../interfaces/home-api.interface';
import { State, Action, StateContext } from '@ngxs/store';
import { FetchHome } from './home.actions';
import { tap } from 'rxjs/operators';

export class HomeStateModel {
    public loading: boolean;
    public api: APIHome | null;
}

@State<HomeStateModel>({
    name: 'home',
    defaults: {
        loading: false,
        api: null
    }
})
export class HomeState {

    constructor(
        protected _wpApi: WPApiService,
    ) { }

    @Action(FetchHome)
    fetchHome(ctx: StateContext<HomeStateModel>) {
        ctx.patchState({ loading: true });
        return this._wpApi.get<APIHome>(`pulsar42/pages/home`).pipe(
            tap((home) => {
                ctx.patchState({ loading: false, api: home });
            })
        )
    }
}
