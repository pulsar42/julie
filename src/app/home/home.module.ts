import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HomeComponent } from './components/home/home.component';
import { FeaturedComponent } from './components/featured/featured.component';
import { HomeState } from './state/home.state';
import { SectionSeparatorComponent } from './components/section-separator/section-separator.component';
import { SeeMoreButtonComponent } from './components/section-separator/see-more/see-more.component';
import { CoreModule } from '../core/core.module';

@NgModule({
    declarations: [
        HomeComponent,
        FeaturedComponent,
        SectionSeparatorComponent,
        SeeMoreButtonComponent
    ],
    imports: [
        FlexLayoutModule,
        CommonModule,
        NgxsModule.forFeature([HomeState]),
        RouterModule.forChild([
            { path: '', component: HomeComponent }
        ]),
        CoreModule,
    ]
})
export class HomeModule { }
