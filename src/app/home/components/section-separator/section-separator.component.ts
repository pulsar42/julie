import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pulsar-section-separator',
  templateUrl: './section-separator.component.html',
  styleUrls: ['./section-separator.component.scss']
})
export class SectionSeparatorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
