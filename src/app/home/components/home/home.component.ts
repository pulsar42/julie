import { FetchHome } from './../../state/home.actions';
import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { HomeStateModel, HomeState } from '../../state/home.state';

@Component({
    selector: 'pulsar-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    @Select(state => state.home.api)
    public state$: Observable<HomeStateModel['api']>;

    constructor(
        protected _store: Store
    ) {
    }
    
    ngOnInit() {
        this._store.dispatch(new FetchHome());
    }

}
