import { APIHomeArticle } from './../../interfaces/home-api.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'pulsar-featured',
    templateUrl: './featured.component.html',
    styleUrls: ['./featured.component.scss']
})
export class FeaturedComponent implements OnInit {
    /** article being displayed */
    @Input("article") public article: APIHomeArticle | null;

    constructor() { }

    ngOnInit() {
    }

}
