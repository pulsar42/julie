import { Directive, ElementRef, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { environment } from '../../../environments/environment';

@Directive({ selector: '[changeLinks]' })
export class changeLinksDirective implements OnInit {
    constructor(private elementRef: ElementRef, private location: Location) { }

    ngOnInit(): void {

        setTimeout(() => { // wait for DOM rendering
            this.reinsertScripts();
        });
    }
    reinsertScripts(): void {
        const anchors = <HTMLAnchorElement[]>this.elementRef.nativeElement.getElementsByTagName('a');
        const anchorsLength = anchors.length;
        for (let i = 0; i < anchorsLength; i++) {
            const anchor = anchors[i];

            if(anchor.href.startsWith(`${environment.selfUrl}#`)) {
                const regex = new RegExp(`${environment.selfUrl}#(.*)`)
                anchor.href = anchor.href.replace(regex, `${this.location.path()}#$1`);
            }
        }
    }
}