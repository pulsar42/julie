import { ArticleState } from './state/article.state';
import { SingleComponent } from './components/single/single.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';
import { SingleHeaderImageComponent } from './components/single-header-image/single-header-image.component';
import { WordpressFullCustomComponent } from './components/wordpress-full-custom/wordpress-full-custom.component';
import { CoreModule } from '../core/core.module';
import { RunScriptsDirective } from './directives/run-script.directive';
import { changeLinksDirective } from './directives/change-links.directive';

@NgModule({
    declarations: [
        SingleComponent,
        SingleHeaderImageComponent,
        WordpressFullCustomComponent,
        RunScriptsDirective,
        changeLinksDirective
    ],
    imports: [
        CommonModule,
        CoreModule,
        NgxsModule.forFeature([ArticleState]),
        RouterModule.forChild([
            { path: '', component: SingleComponent, pathMatch: 'full' }
        ])
    ]
})
export class SingleModule { }
