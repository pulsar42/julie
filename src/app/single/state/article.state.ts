import { CoreState, CoreStateModel } from './../../core/state/core.state';
import { FeaturedState } from './../../core/state/index';
import { WPApiService } from './../../core/services/wp-api.service';
import { APISingleArticle } from './../interfaces/articles.interface';
import { State, Action, StateContext, Selector, createSelector } from '@ngxs/store';
import { FetchArticleBySlug, FetchRelatedArticles, FetchArticleById } from './article.actions';
import { tap, map, catchError } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { RootState } from '../../core/state';
import { Observable, of } from 'rxjs';
import { ArticleAuthors } from '../interfaces/article-authors.interface';

export class ArticleStateModel {
    public currentArticle: APISingleArticle | null;
    public relatedArticles: APISingleArticle[] | null;
    public loading: boolean;
}

@State<ArticleStateModel>({
    name: 'article',
    defaults: {
        currentArticle: null,
        relatedArticles: null,
        loading: true,
    }
})
export class ArticleState {

    constructor(
        protected _wpApi: WPApiService,
        protected _sanitizer: DomSanitizer
    ) { }

    @Selector([CoreState, ArticleState])
    public static selectAuthorsForArticle(articleState: ArticleStateModel, coreState: CoreStateModel): ArticleAuthors | undefined {
        if (!articleState.currentArticle) return;

        if (articleState.currentArticle.type === "post") {
            return { isTrad: false, poster: coreState.wpStaff.find((u) => Number(u.id) === Number(articleState.currentArticle.author)) }
        }
        else if (articleState.currentArticle.type === "traduction") {
            if (articleState.currentArticle.meta && !Array.isArray(articleState.currentArticle.meta)) {
                const metas = articleState.currentArticle.meta;
                return {
                    isTrad: true,
                    translators: coreState.wpStaff.filter((u) => metas.translator.includes(Number(u.id))),
                    proofReaders: coreState.wpStaff.filter((u) => metas.proofReader.includes(Number(u.id))),
                    integrators: coreState.wpStaff.filter((u) => metas.integrator.includes(Number(u.id)))
                }
            }
        }
        else return;
    }

    @Action(FetchArticleBySlug)
    @Action(FetchArticleById)
    protected fetchArticle(ctx: StateContext<ArticleStateModel>, action: FetchArticleBySlug | FetchArticleById) {
        const state = ctx.getState();
        ctx.patchState({ loading: true })

        const search = (action as FetchArticleById).id ? `include[]=${(action as FetchArticleById).id}&status=any` : `slug=${(action as FetchArticleBySlug).slug}`

        if (
            ( (action as FetchArticleBySlug).slug && state.currentArticle && state.currentArticle.slug !== (action as FetchArticleBySlug).slug )
            || (action as FetchArticleById).id > 0
        ) {
            ctx.patchState({ currentArticle: null, relatedArticles: null });
        }

        return this._wpApi.get<APISingleArticle[]>(`wp/v2/multiple-post-type?type[]=post&type[]=traduction&${search}`).pipe(
            map((article) => {
                if (article && article[0]) {
                    article[0].content.safe = this._sanitizer.bypassSecurityTrustHtml(article[0].content.rendered);
                    return ctx.patchState({ currentArticle: article[0], loading: false })
                }
            }),
            catchError((error, caught) => {
                // tried to access a preview but unauthorized
                if((action as FetchArticleById).id && error.code === "rest_invalid_param") {

                }

                console.error("ah", error);

                return of()
            })
        );
    }

    @Action(FetchRelatedArticles)
    protected fetchRelatedArticles(ctx: StateContext<ArticleStateModel>) {
        if(!ctx.getState().currentArticle) return;

        const currentArticleId = Number(ctx.getState().currentArticle.id);

        return this._wpApi.get<APISingleArticle[]>(`pulsar42/single/related/${currentArticleId}`).pipe(
            map((relatedArticles) => {
                return ctx.patchState({ relatedArticles })
            }));
    }
}
