/**
 * Fetch a given article by its slug name
 */
export class FetchArticleBySlug {
    static readonly type = '[Article] Fetch article by slug';
    constructor(public slug: string) { }
}

/**
 * Fetch a given article by its ID
 */
export class FetchArticleById {
    static readonly type = '[Article] Fetch article by id';
    constructor(public id: number) { }
}

/**
 * Fetch articles related to the one we're currently viewing
 */
export class FetchRelatedArticles {
    static readonly type = '[Article] Fetch Related articles';
}