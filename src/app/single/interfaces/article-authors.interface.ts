import { WPUserStaff } from "../../core/interfaces/wp-user.interface";

export type ArticleAuthors = ArticleRedacAuthor | ArticleTradAuthors;

export interface ArticleRedacAuthor {
    isTrad: false;
    poster: WPUserStaff
}

export interface ArticleTradAuthors {
    isTrad: true;
    translators: WPUserStaff[];
    proofReaders: WPUserStaff[];
    integrators: WPUserStaff[];
}