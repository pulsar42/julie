import { APISingleArticle } from './../interfaces/articles.interface';
import { APIHomeArticle } from './../../home/interfaces/home-api.interface';
import { UrlSegment, UrlMatchResult, UrlSegmentGroup, Route } from '@angular/router';
import * as moment from 'moment';

/**
 * URL Matcher for our articles
 * matches `:year/:month/:day/:slug`
 *
 * ex: `2019/01/23/an-article`
 */
export function singleArticleMatcher(segments: UrlSegment[], group: UrlSegmentGroup, route: Route): UrlMatchResult {
    if (segments.length !== 4) { return null; }

    if (
        // year
        isNaN(Number(segments[0].path)) || segments[0].path.length !== 4
        // month
        || isNaN(Number(segments[1].path)) || segments[1].path.length !== 2
        // day
        || isNaN(Number(segments[2].path)) || segments[2].path.length !== 2
        // slug
        || !segments[3].path.length
    ) {
        return null;
    }

    return { consumed: segments, posParams: { year: segments[0], month: segments[1], day: segments[2], slug: segments[3] } };
};

/**
 * Generate the navigation link for a given article
 */
export function singleArticleLinkGenerator(article: APIHomeArticle | APISingleArticle): string[] {
    const targetName = (article as APIHomeArticle).post_name ? (article as APIHomeArticle).post_name : (article as APISingleArticle).slug;
    const targetDate = (article as APIHomeArticle).post_date_gmt ? (article as APIHomeArticle).post_date_gmt : (article as APISingleArticle).date_gmt;
    const date = moment(targetDate, "YYYY-MM-DD HH:mm:SS");
    return ["/", date.format("YYYY"), date.format("MM"), date.format("DD"), targetName];
}