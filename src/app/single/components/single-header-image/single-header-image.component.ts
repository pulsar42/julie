import { Component, OnInit, Input } from '@angular/core';
import { APISingleArticle } from '../../interfaces/articles.interface';

@Component({
    selector: 'pulsar-single-header-image',
    templateUrl: './single-header-image.component.html',
    styleUrls: ['./single-header-image.component.scss']
})
export class SingleHeaderImageComponent implements OnInit {

    @Input() article: APISingleArticle | null;

    constructor() { }

    ngOnInit() {
    }

}
