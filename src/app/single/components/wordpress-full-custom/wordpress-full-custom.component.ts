import { APISingleArticle } from '../../../single/interfaces/articles.interface';
import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
    selector: 'pulsar-wordpress-full-custom',
    templateUrl: './wordpress-full-custom.component.html',
    styleUrls: ['./wordpress-full-custom.component.scss'],
    encapsulation: ViewEncapsulation.Emulated
})
export class WordpressFullCustomComponent implements OnInit {
    @Input() article: APISingleArticle;

    constructor() { }

    ngOnInit() {
    }

}
