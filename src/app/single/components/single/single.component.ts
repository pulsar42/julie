import { ArticleState } from './../../state/article.state';
import { FetchArticleBySlug, FetchRelatedArticles, FetchArticleById } from './../../state/article.actions';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, Select } from '@ngxs/store';
import { APISingleArticle } from '../../interfaces/articles.interface';
import { Observable } from 'rxjs';
import { FeaturedState } from '../../../core/state';
import { MetaService } from '@ngx-meta/core';
import { ArticleAuthors } from '../../interfaces/article-authors.interface';

@Component({
    selector: 'pulsar-single',
    templateUrl: './single.component.html',
    styleUrls: ['./single.component.scss']
})
export class SingleComponent implements OnInit {

    @Select((state: FeaturedState) => state.article.currentArticle)
    public article$: Observable<APISingleArticle>;

    @Select(ArticleState.selectAuthorsForArticle)
    public authors$: Observable<ArticleAuthors>

    @Select((state: FeaturedState) => state.article.relatedArticles)
    public relatedArticles$: Observable<APISingleArticle>

    constructor(
        protected _activatedRoute: ActivatedRoute,
        protected _store: Store,
        protected _meta: MetaService
    ) {

        this._activatedRoute.params.subscribe((params) => {
            if (params.slug) {
                this.doFetching(new FetchArticleBySlug(params['slug']))
            } else if (params.id) {
                this.doFetching(new FetchArticleById(Number(params['id'])))
            }
        })

        this.article$.subscribe((article) => {
            if(!article) return;
            const title = article.title.rendered.replace(/<[^>]*>?/gm, '');
            const excerpt = article.uagb_excerpt.replace(/<[^>]*>?/gm, '');

            this._meta.setTitle(`Pulsar 42 - ${title}`, true);
            this._meta.setTag('og:title', `Pulsar42 - ${title}`);
            this._meta.setTag('og:description', excerpt);
            this._meta.setTag('og:image', article.thumbnail);
        })
    }

    protected doFetching(action: FetchArticleBySlug | FetchArticleById): void {
        this._store.dispatch(action).subscribe(() => {
            this._store.dispatch(new FetchRelatedArticles())
        });
    }

    
    ngOnInit() {
    }

}
