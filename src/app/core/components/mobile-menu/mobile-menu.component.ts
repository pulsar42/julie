import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { RootState } from '../../state';
import { Observable } from 'rxjs';
import { WPMenu } from '../../interfaces/wp-menu.interface';
import { take } from 'rxjs/operators';
import { ToggleTheme } from '../../state/core.actions';

@Component({
    selector: 'pulsar-mobile-menu',
    templateUrl: './mobile-menu.component.html',
    styleUrls: ['./mobile-menu.component.scss']
})
export class MobileMenuComponent {

    /** if we're currently in dark state mode */
    @Select((state: RootState) => state.core.darkTheme)
    darkTheme$: Observable<boolean>;

    /** data for the menu we're displaying */
    @Select((state: RootState) => state.core.menu)
    menu$: Observable<WPMenu>;

    constructor(protected _store: Store) {

    }

    public toggleTheme() {
        this.darkTheme$.pipe(take(1)).subscribe((darkMode) => {
            this._store.dispatch(new ToggleTheme(!darkMode));
        });
    }
}
