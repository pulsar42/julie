import { Observable } from 'rxjs';
import { FeaturedState } from './../../../state/index';
import { CheckTwitch } from './../../../state/core.actions';
import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { DateTwitchStream } from '../../../interfaces/twitch-stream.interface';

@Component({
    selector: 'pulsar-twitch-live',
    templateUrl: './twitch-live.component.html',
    styleUrls: ['./twitch-live.component.scss']
})
export class TwitchLiveComponent implements OnInit {

    @Select((state: FeaturedState) => state.core.twitch)
    public twitch$: Observable<DateTwitchStream>

    constructor(
        protected _store: Store
    ) { }

    ngOnInit() {
        this._store.dispatch(new CheckTwitch());
    }

}

