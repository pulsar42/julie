import { WPMenu } from './../../interfaces/wp-menu.interface';
import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { RootState } from '../../state';
import { Observable } from 'rxjs';
import { ToggleTheme, FetchMenu, ToggleSideNav } from '../../state/core.actions';
import { take } from 'rxjs/operators';

@Component({
    selector: 'pulsar-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

    /** if we're currently in dark state mode */
    @Select((state: RootState) => state.core.darkTheme)
    darkTheme$: Observable<boolean>;

    /** data for the menu we're displaying */
    @Select((state: RootState) => state.core.menu)
    menu$: Observable<WPMenu>;

    constructor(protected _store: Store) { 
        this._store.dispatch(new FetchMenu());

    }

    public toggleTheme() {
        this.darkTheme$.pipe(take(1)).subscribe((darkMode) => {
            this._store.dispatch(new ToggleTheme(!darkMode));
        });
    }

    public toggleMenu() {
        this._store.dispatch(new ToggleSideNav())
    }

}
