import { Component, OnInit, Input } from '@angular/core';
import { APIHomeEvent } from '../../../home/interfaces/home-api.interface';

@Component({
    selector: 'pulsar-event-featured',
    templateUrl: './event-featured.component.html',
    styleUrls: ['./event-featured.component.scss']
})
export class EventFeaturedComponent implements OnInit {
    @Input() event: APIHomeEvent;

    constructor() { }

    ngOnInit() {
    }

}
