import { WPBreadcrumbs } from './../../interfaces/wp-breadcrumbs.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'pulsar-breadcrumbs',
    templateUrl: './breadcrumbs.component.html',
    styleUrls: ['./breadcrumbs.component.scss']
})
export class BreadcrumbsComponent implements OnInit {

    @Input() breadcrumbs: WPBreadcrumbs;
    
    constructor() { }

    ngOnInit() {
    }

}
