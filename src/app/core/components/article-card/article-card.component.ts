import { WPCategory } from './../../interfaces/wp-category.interface';
import { CoreState } from './../../state/core.state';
import { APISingleArticle } from './../../../single/interfaces/articles.interface';
import { Component, OnInit, Input } from '@angular/core';
import { APIHomeArticle, APIHomeArticleCategory } from '../../../home/interfaces/home-api.interface';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
    selector: 'pulsar-article-card',
    templateUrl: './article-card.component.html',
    styleUrls: ['./article-card.component.scss']
})
export class ArticleCardComponent implements OnInit {
    /** article being displayed */
    @Input("article") public article: APIHomeArticle | APISingleArticle | null;

    /** categories of this article */
    public categories$!: Observable<WPCategory[]>;

    public get date() {
        return this.article &&
            (this.article as APIHomeArticle).post_date ?
                (this.article as APIHomeArticle).post_date :
                (this.article as APISingleArticle).date;
    }

    public get title() {
        return this.article &&
            (this.article as APIHomeArticle).post_title != null ?
                (this.article as APIHomeArticle).post_title :
                (this.article as APISingleArticle).title.rendered;
    }

    public get excerpt() {
        return this.article &&
            (this.article as APIHomeArticle).post_excerpt ?
                (this.article as APIHomeArticle).post_excerpt :
                (this.article as APISingleArticle).uagb_excerpt;
    }

    constructor(
        protected _store: Store
    ) {

     }

    ngOnInit() {
        if(this.article) {
            this.categories$ = this._store.select(
                CoreState.categoriesById(
                    (this.article.categories as (number|APIHomeArticleCategory)[])
                        .map((cat) => {
                            if (typeof cat === "number" || !isNaN(Number(cat))) {
                                return Number(cat)
                            }
                            if ((cat as APIHomeArticleCategory).id) {
                                return Number((cat as APIHomeArticleCategory).id)
                            }
                         })
                )
            )
        }
    }

}
