import { Component, OnInit, Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
    selector: 'pulsar-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

    @Input() id: string;
    @Output("pageChange") pageChange: EventEmitter<number> = new EventEmitter();

    constructor() { }

    ngOnInit() {
    }

    public pageChanged(number: number) {
        this.pageChange.emit(number as any);
    }


}
