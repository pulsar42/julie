import { APISingleArticle } from './../../../single/interfaces/articles.interface';
import { Component, OnInit, Input } from '@angular/core';
import { APIHomeArticle } from '../../../home/interfaces/home-api.interface';

@Component({
    selector: 'pulsar-featurette',
    templateUrl: './featurette.component.html',
    styleUrls: ['./featurette.component.scss']
})
export class FeaturetteComponent implements OnInit {
    /** article being displayed */
    @Input("article") public article: APIHomeArticle | APISingleArticle | null;


    public get title() {
        return this.article &&
            (this.article as APIHomeArticle).post_title ?
                (this.article as APIHomeArticle).post_title :
                (this.article as APISingleArticle).title.rendered;
    }

    public get excerpt() {
        return this.article &&
            (this.article as APIHomeArticle).post_excerpt ?
                (this.article as APIHomeArticle).post_excerpt :
                (this.article as APISingleArticle).uagb_excerpt;
    }

    public get thumbnail() {
        return this.article &&
        (this.article as APIHomeArticle).thumbnail ?
            (this.article as APIHomeArticle).thumbnail :
            (this.article as APISingleArticle).uagb_featured_image_src ?
            (this.article as APISingleArticle).uagb_featured_image_src.medium_large[0] :
            ""
    }

    constructor() { }

    ngOnInit() {
    }

}
