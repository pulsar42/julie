import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
    selector: 'pulsar-wordpress-content-wrapper',
    templateUrl: './wordpress-content-wrapper.component.html',
    styleUrls: ['./wordpress-content-wrapper.component.scss']
})
export class WordpressContentWrapperComponent implements OnInit {

    public wpUrls!: SafeResourceUrl[];

    @Input() content: string = "";

    constructor(protected _sanitizer: DomSanitizer) {
        this.wpUrls = [
            this._sanitizer.bypassSecurityTrustResourceUrl(`${environment.wordpress.url}wp-includes/css/dist/block-library/theme.min.css?ver=5.2.2`),
            this._sanitizer.bypassSecurityTrustResourceUrl(`${environment.wordpress.url}wp-content/plugins/ultimate-addons-for-gutenberg/dist/blocks.style.build.css?ver=1.13.1`),
            this._sanitizer.bypassSecurityTrustResourceUrl(`${environment.wordpress.url}wp-includes/css/dist/block-library/style.min.css?ver=5.0.3`),
        ];
    }

    ngOnInit() {
    }

}
