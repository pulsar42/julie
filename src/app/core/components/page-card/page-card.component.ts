import { WPPage } from './../../../page/interfaces/wp-page.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'pulsar-page-card',
    templateUrl: './page-card.component.html',
    styleUrls: ['./page-card.component.scss']
})
export class PageCardComponent implements OnInit {
    @Input() page: WPPage;

    constructor() { }

    ngOnInit() {
    }

}
