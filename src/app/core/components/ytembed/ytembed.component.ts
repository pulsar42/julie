import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import YouTubePlayer from 'youtube-player';

@Component({
    selector: 'pulsar-ytembed',
    templateUrl: './ytembed.component.html',
    styleUrls: ['./ytembed.component.scss']
})
export class YtembedComponent implements OnInit, OnChanges {

    protected player;
    public muted: boolean = true;

    @Input() public ytCode: string;

    ngOnInit() {
        if(this.ytCode) {
            this.initYT()
        }
    }

    ngOnChanges(changes:SimpleChanges) {
        if(changes.yTcode && changes.yTcode.currentValue && changes.yTcode.currentValue != changes.yTcode.previousValue) {
            this.initYT();
        }
    }

    public initYT() {
        this.player = YouTubePlayer('player');
        this.player.loadVideoById(this.ytCode);
        this.player.mute();
        this.player.on('stateChange', (e) => {
            if (e.data === 0) {
                this.player.playVideo(); 
            }
        })
    }

    public toggleMute() {
        if(this.muted) this.player.unMute();
        else this.player.mute();

        this.muted = !this.muted;
    }

}
