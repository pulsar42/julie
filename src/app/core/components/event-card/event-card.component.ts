import { APIHomeEvent } from './../../../home/interfaces/home-api.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'pulsar-event-card',
    templateUrl: './event-card.component.html',
    styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent {
    
    @Input() event: APIHomeEvent;

    constructor() { }


}
