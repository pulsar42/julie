import { APICategory } from './../../archive/interfaces/api-category.interface';
import { Pipe, PipeTransform } from '@angular/core';
import { archiveCategoryLinkGenerator } from '../../archive/matchers/archive.matcher';

/**
 * Transform a category into an angular link
 */
@Pipe({
    name: 'categoryLink'
})
export class CategoryLinkPipe implements PipeTransform {

    transform(value: { id: number, slug: string }): string[] {
        return archiveCategoryLinkGenerator(value);
    }

}
