import { WPPage } from './../../page/interfaces/wp-page.interface';
import { Pipe, PipeTransform } from '@angular/core';
import { generateLinkForPage } from '../../page/matchers/page.matcher';

/**
 * Transform a page into an angular link
 */
@Pipe({
    name: 'pageLink'
})
export class PageLinkPipe implements PipeTransform {

    transform(value: WPPage): string[] {
        return generateLinkForPage(value);
    }

}
