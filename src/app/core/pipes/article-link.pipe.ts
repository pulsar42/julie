import { APIHomeArticle } from './../../home/interfaces/home-api.interface';
import { Pipe, PipeTransform } from '@angular/core';
import { singleArticleLinkGenerator } from '../../single/matchers/article.matcher';

/**
 * Transform an article into an angular link
 */
@Pipe({
    name: 'articleLink'
})
export class ArticleLinkPipe implements PipeTransform {

    transform(value: APIHomeArticle): string[] {
        return singleArticleLinkGenerator(value);
    }

}
