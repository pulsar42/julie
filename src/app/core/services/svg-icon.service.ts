import { Injectable } from "@angular/core";
import { MatIconRegistry } from "@angular/material";
import { DomSanitizer } from "@angular/platform-browser";

@Injectable({
    providedIn: "root"
})
export class SvgIconService {
    constructor(
        private matIconRegistry: MatIconRegistry,
        private domSanitizer: DomSanitizer
    ) {
        customWojtekIcons.forEach(icon => {
            this.matIconRegistry.addSvgIcon(
                icon.iconName,
                this.domSanitizer.bypassSecurityTrustResourceUrl(icon.url)
            );
        });
    }
}

const customWojtekIcons: { iconName: string; url: string }[] = [
    {
        iconName: "youtube",
        url: "/assets/icons/youtube.svg"
    },
    {
        iconName: "facebook",
        url: "/assets/icons/facebook.svg"
    },
    {
        iconName: "twitch",
        url: "/assets/icons/twitch.svg"
    },
    {
        iconName: "twitter",
        url: "/assets/icons/twitter.svg"
    }
];
