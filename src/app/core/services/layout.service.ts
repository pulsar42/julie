import { Injectable, Renderer2, Inject, RendererFactory2 } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Store } from '@ngxs/store';
import { RootState } from '../state';

@Injectable({
    providedIn: 'root'
})
export class LayoutService {
    protected _renderer!: Renderer2;
    constructor(
        @Inject(DOCUMENT) protected _document: Document,
        protected _store: Store,
        rendererFactory: RendererFactory2
    ) {
        this._renderer = rendererFactory.createRenderer(null, null);
        setTimeout(() => {
            this._store.select((state: RootState) => state.core.darkTheme).subscribe((darkTheme) => {
                this.setTheme(darkTheme);
            })
        })
    }

    /**
     * Toggle dark theme on/off
     */
    public setTheme(dark: boolean) {
        if (dark) {
            this._renderer.addClass(this._document.body, 'dark')
            this._renderer.removeClass(this._document.body, 'light')
        } else {
            this._renderer.addClass(this._document.body, 'light')
            this._renderer.removeClass(this._document.body, 'dark')
        }
    }
}
