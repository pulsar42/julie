import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { RootState } from '../state';
import { catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class WPApiService {

    protected _wpApi = `${environment.wordpress.url}wp-json/`;

    constructor(
        protected _http: HttpClient,
        protected _store: Store
    ) {

    }

    public get<T = any>(url: string, params?: {
        [param: string]: string | string[];
    }, observe?: 'body'): Observable<T>
    public get<T = any>(url: string, params?: {
        [param: string]: string | string[];
    }, observe?: 'response'): Observable<HttpResponse<T>>
    public get<T = any>(url: string, params: {
        [param: string]: string | string[];
    } = {}, observe?: 'response' | 'body') {
        const headers = {};
        const JWT = this._store.selectSnapshot((state: RootState) => state.core.jwt)

        if(JWT) headers['Authorization'] = `Bearer ${JWT}`;
        return this._http
            .get<T>(`${this._wpApi}${url}`, { params, observe: observe as any, headers })
    }
}
