import { CornerComponent } from './corner/corner.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { WordpressContentWrapperComponent } from './components/wordpress-content-wrapper/wordpress-content-wrapper.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { ArticleCardComponent } from './components/article-card/article-card.component';
import { MomentModule } from "ngx-moment";
import { MatIconModule, MatButtonModule, MatTooltipModule, MatSidenavModule } from "@angular/material";
import { ArticleLinkPipe } from './pipes/article-link.pipe';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './components/footer/footer.component';
import { EventCardComponent } from './components/event-card/event-card.component';
import { EventFeaturedComponent } from './components/event-featured/event-featured.component';
import { EventTypePipe } from './pipe/event-type.pipe';
import { YtembedComponent } from './components/ytembed/ytembed.component';
import { FeaturetteComponent } from './components/featurette/featurette.component';
import { CategoryLinkPipe } from './pipes/category-link.pipe';
import { PaginationComponent } from './components/pagination/pagination.component';
import { BreadcrumbsComponent } from './components/breadcrumbs/breadcrumbs.component';
import { PageCardComponent } from './components/page-card/page-card.component';
import { PageLinkPipe } from './pipes/page-link.pipe';
import { TwitchLiveComponent } from './components/header/twitch-live/twitch-live.component';
import { MobileMenuComponent } from './components/mobile-menu/mobile-menu.component';

@NgModule({
    declarations: [
        HeaderComponent, 
        ArticleCardComponent, 
        ArticleLinkPipe, 
        FooterComponent, 
        EventCardComponent, 
        EventFeaturedComponent, 
        EventTypePipe,
        YtembedComponent,
        FeaturetteComponent,
        CategoryLinkPipe,
        PaginationComponent,
        WordpressContentWrapperComponent,
        BreadcrumbsComponent,
        PageCardComponent,
        PageLinkPipe,
        TwitchLiveComponent,
        CornerComponent,
        MobileMenuComponent,
    ],
    imports: [
        CommonModule,
        MomentModule,
        MatIconModule,
        MatButtonModule,
        MatSidenavModule,
        RouterModule,
        NgxPaginationModule,
        FlexLayoutModule,
        MatTooltipModule,
    ],
    exports: [
        HeaderComponent, 
        ArticleCardComponent, 
        MomentModule,
        MatIconModule,
        MatSidenavModule,
        MatButtonModule,
        MatTooltipModule,
        ArticleLinkPipe,
        FooterComponent,
        EventCardComponent,
        EventFeaturedComponent,
        EventTypePipe,
        YtembedComponent,
        FeaturetteComponent,
        CategoryLinkPipe,
        PaginationComponent,
        WordpressContentWrapperComponent,
        BreadcrumbsComponent,
        PageCardComponent,
        PageLinkPipe,
        CornerComponent,
        MobileMenuComponent,
    ]
})
export class CoreModule {
    static forRoot(): ModuleWithProviders<CoreModule> {
        return {
            ngModule: CoreModule,
            providers: []
        };
    }
}
