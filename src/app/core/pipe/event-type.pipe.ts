import { APIHomeEventType } from './../../home/interfaces/home-api.interface';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'eventType'
})
export class EventTypePipe implements PipeTransform {

    transform(value: APIHomeEventType): string {
        switch(value.toUpperCase()) {
            case "BAR_CITIZEN":
                return "Bar Citizen";
            case "MISC":
                return "Autre";
            case "OFFICIAL":
                return "Officiel";
            case "PARIVERSE":
                return "Pari'verse";
        }
    }

}
