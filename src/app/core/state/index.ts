import { ArchiveStateModel } from './../../archive/state/archive.state';
import { PageStateModel } from './../../page/state/page.state';
import { CoreStateModel } from './core.state';
import { ArticleStateModel } from '../../single/state/article.state';
import { SearchStateModel } from '../../single-search/state/search.state';

export interface RootState {
    core: CoreStateModel
}

export interface FeaturedState extends RootState {
    article: ArticleStateModel,
    page: PageStateModel,
    archive: ArchiveStateModel,
    search: SearchStateModel,
}