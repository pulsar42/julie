import { DateTwitchStream, TwitchStream } from './../interfaces/twitch-stream.interface';
import { WPApiService } from './../services/wp-api.service';
import { WPMenu, Item } from './../interfaces/wp-menu.interface';
import { LayoutService } from './../services/layout.service';
import { State, Action, StateContext, createSelector } from '@ngxs/store';
import { ToggleTheme, FetchMenu, CheckTwitch, FetchStaff, FetchCategories, ToggleSideNav, SetRestWPJWT as SetRestWPJWT } from './core.actions';
import { map } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { archiveCategoryLinkGenerator } from '../../archive/matchers/archive.matcher';
import { HttpClient } from '@angular/common/http';
import { WPUserStaff } from '../interfaces/wp-user.interface';
import { WPCategory } from '../interfaces/wp-category.interface';

export class CoreStateModel {
    public darkTheme: boolean;
    public menu: WPMenu | null;
    public twitch: DateTwitchStream | null;
    public wpStaff: WPUserStaff[];
    public categories: WPCategory[];
    public sidenav: boolean;
    public jwt?: string;
}

@State<CoreStateModel>({
    name: 'core',
    defaults: {
        darkTheme: false,
        menu: null,
        twitch: null,
        wpStaff: [],
        categories: [],
        sidenav: false,
        jwt: undefined,
    }
})
export class CoreState {

    constructor(
        protected _layout: LayoutService,
        protected _http: HttpClient,
        protected _wp: WPApiService,
    ) { }

    
    public static categoriesById(categoriesId: number[]): (...args: any[]) => WPCategory[] {
        return createSelector([CoreState], (coreState: CoreStateModel) => {
            return coreState.categories.filter((cat) =>   categoriesId.includes(Number(cat.cat_ID)) )
        })
    }

    @Action(ToggleTheme)
    toggleTheme(ctx: StateContext<CoreStateModel>, action: ToggleTheme) {
        return ctx.patchState({ darkTheme: Boolean(action.darkTheme) });
    }

    @Action(FetchMenu)
    fetchMenu(ctx: StateContext<CoreStateModel>) {
        return this._wp.get<WPMenu>('menus/v1/menus/main').pipe(
            map((menu) => {

                const addRouterLink = (i: Item) => {
                    // IF its a link meant to be followed inside the app
                    if (i.url.includes(environment.wordpress.url)) {
                        let url = i.url.replace(environment.wordpress.url.slice(0, environment.wordpress.url.length - 1), '');
                        // then we'll convert it to routerlink
                        const cat = url.match('/category/([^/]*)');
                        if (cat && cat.length > 0) {
                            i.routerLink = archiveCategoryLinkGenerator({ id: Number(i.object_id), slug: cat[1] });
                        } else {
                            i.routerLink = [url];
                        }
                    }

                    if (i.child_items && i.child_items.length) {
                        i.child_items = i.child_items.map(si => addRouterLink(si))
                    }

                    return i;
                }

                if (menu && menu.items) {
                    menu.items = Object.values(menu.items).map((item) => {
                        return addRouterLink(item);
                    })
                    return ctx.patchState({ menu });
                } else {
                    // TO DO error
                }
            })
        )
    }

    @Action(CheckTwitch)
    checkTwitch(ctx: StateContext<CoreStateModel>) {
        // const last = ctx.getState().twitch ? ctx.getState().twitch.dateChecked : null;

        // if (!last || moment(last).isBefore(moment().subtract(15, "minutes"))) {
            return this._http.get<TwitchStream>(`https://api.twitch.tv/helix/streams?user_login=pulsar42_sc`, { headers: { "Client-ID": "69488dnqo8dee206mvgp3icy25wpx4" } }).pipe(
                map((twitch) => {
                    return ctx.patchState({ twitch: { ...twitch, dateChecked: new Date() } })
                })
            )
        // }
    }

    @Action(FetchStaff)
    FetchStaff(ctx: StateContext<CoreStateModel>) {
        return this._wp.get<WPUserStaff[]>('pulsar42/admin/users_for_trad').pipe(
            map((data) => {
                ctx.patchState({ wpStaff: data })
            })
        )
    }

    @Action(FetchCategories)
    FetchCategories(ctx: StateContext<CoreStateModel>) {
        return this._wp.get<WPCategory[]>('pulsar42/single/categories').pipe(
            map((data) => {
                ctx.patchState({ categories: data })
            })
        )
    }

    @Action(ToggleSideNav)
    toggleSideNav(ctx: StateContext<CoreStateModel>, action: ToggleSideNav) {
        const current = ctx.getState().sidenav

        return ctx.patchState({
            sidenav: action.setMode !== undefined ? action.setMode : !current
        })
    }

    @Action(SetRestWPJWT)
    setRestWPNonce(ctx: StateContext<CoreStateModel>, action: SetRestWPJWT) {
        return ctx.patchState({ jwt: action.jwt ? action.jwt : undefined })
    }
}
