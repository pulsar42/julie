/**
 * Toggle between light and dark theme
 */
export class ToggleTheme {
    static readonly type = '[Core] Toggle Theme';
    constructor(public darkTheme: boolean) { }
}


/**
 * Fetch menu from back-end
 */
export class FetchMenu {
    static readonly type = '[Core] Fetch menu';
}

/**
 * Check the status of twitch live
 */
export class CheckTwitch {
    static readonly type = "[Core] Check Twitch"
}

/**
 * Fetch staff that can post on the w.p
 */
export class FetchStaff {
    static readonly type = "[Core] Fetch staff"
}

/**
 * Fetch all categories
 */
export class FetchCategories {
    static readonly type = "[Core] Fetch Categories"
}

/**
 * Toggle sidenav menu
 */
export class ToggleSideNav {
    static readonly type = "[Core] Toggle Sidenav"

    constructor(public setMode?: boolean) {}
}

/**
 * Save a nonce as being the current one used for any wp api calls
 */
export class SetRestWPJWT {
    static readonly type = "[Core] Set WP Rest API JWT"
    constructor(public readonly jwt: string) {}
}