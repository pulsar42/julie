export interface WPUserStaff {
    id:          number;
    name:        string;
    url:         string;
    description: string;
    link:        string;
    slug:        string;
    avatar_urls: { [key: string]: string };
    acf:         Acf;
    _links:      Links;
}

export interface Links {
    self:       Collection[];
    collection: Collection[];
}

export interface Collection {
    href: string;
}

export interface Acf {
    traducteur:  boolean;
    redacteur:   boolean;
    relector?:   boolean;
    integrateur: boolean;
}
