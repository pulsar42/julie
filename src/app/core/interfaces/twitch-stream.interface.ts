
export interface TwitchStream {
    data: Datum[];
    pagination: Pagination;
}

export interface Datum {
    id: string;
    user_id: string;
    user_name: string;
    game_id: string;
    community_ids: any[];
    type: string;
    title: string;
    viewer_count: number;
    started_at: string;
    language: string;
    thumbnail_url: string;
    tag_ids: string[];
}

export interface Pagination {
    cursor: string;
}


export interface DateTwitchStream extends TwitchStream {
    dateChecked: Date
}