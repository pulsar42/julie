
/**
 * Breadcrumbs as returned by WP
 */
export interface WPBreadcrumbs {
    "@context":      string;
    "@type":         string;
    itemListElement: ItemListElement[];
}

export interface ItemListElement {
    "@type":  string;
    position: number;
    item:     Item;
}

export interface Item {
    "@id": string;
    name:  string;
}
