export interface WPCategory {
    term_id:              number;
    name:                 string;
    slug:                 string;
    term_group:           number;
    term_taxonomy_id:     number;
    taxonomy:             Taxonomy;
    description:          string;
    parent:               number;
    count:                number;
    filter:               Filter;
    cat_ID:               number;
    category_count:       number;
    category_description: string;
    cat_name:             string;
    category_nicename:    string;
    category_parent:      number;
}

export enum Filter {
    Raw = "raw",
}

export enum Taxonomy {
    Category = "category",
}
