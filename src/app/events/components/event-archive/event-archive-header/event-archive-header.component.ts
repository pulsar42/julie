import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'pulsar-event-archive-header',
  templateUrl: './event-archive-header.component.html',
  styleUrls: ['./event-archive-header.component.scss']
})
export class EventArchiveHeaderComponent implements OnInit {

    public category$;

  constructor() { }

  ngOnInit() {
  }

}
