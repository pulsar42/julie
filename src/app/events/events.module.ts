import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EventPageComponent } from './components/event-page/event-page.component';
import { RouterModule } from '@angular/router';
import { EventArchiveComponent } from './components/event-archive/event-archive.component';
import { EventArchiveHeaderComponent } from './components/event-archive/event-archive-header/event-archive-header.component';

@NgModule({
    declarations: [EventPageComponent, EventArchiveComponent, EventArchiveHeaderComponent],
    imports: [
        CommonModule,
        RouterModule.forChild([
            { path: '', pathMatch: 'full', component: EventArchiveComponent },
            { path: ':pageNumber', pathMatch: 'full', component: EventArchiveComponent }
        ])
    ]
})
export class EventsModule { }
