import { FetchStaff, FetchCategories, ToggleSideNav, SetRestWPJWT } from './core/state/core.actions';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, Renderer2, ViewChild, AfterViewInit } from '@angular/core';
import * as moment from 'moment';
import { SvgIconService } from './core/services/svg-icon.service';
import { NavigationEnd } from "@angular/router";
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { RootState } from './core/state';
import { MatSidenav } from '@angular/material';
moment.locale('fr');
declare let gtag: Function;

@Component({
    selector: 'app-root',
    template: `<mat-sidenav-container>
    <mat-sidenav mode="over" [opened]="sidenav$ | async"><pulsar-mobile-menu></pulsar-mobile-menu></mat-sidenav>
        <mat-sidenav-content>
        <pulsar-header></pulsar-header>
        <router-outlet></router-outlet>
        <pulsar-footer></pulsar-footer>
        </mat-sidenav-content>
    </mat-sidenav-container>`,
    styles: []
})
export class AppComponent implements AfterViewInit {

    @Select((state: RootState) => state.core.sidenav)
    public sidenav$: Observable<boolean>;

    @ViewChild(MatSidenav)
    protected _matSideNav: MatSidenav

    constructor(
        protected _svgIcon: SvgIconService,
        protected _router: Router,
        protected _store: Store,
        protected _renderer: Renderer2,
        protected _activatedroute: ActivatedRoute
    ) {
        this._activatedroute.queryParams.subscribe((qp) => {
            if (qp.jwt) {
                this._store.dispatch(new SetRestWPJWT(qp.jwt))
                window.location.href = this._router.url.replace(/\?.*/, '')
            }
        })

        this._router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                gtag('config', 'UA-123787103-2', { 'page_path': event.urlAfterRedirects });
            }
            this._store.dispatch(new ToggleSideNav(false))

            if (window && window.scroll) {
                const html = Array.from(document.getElementsByTagName('html'))[0]
                html.style.scrollBehavior = "auto";
                window.scroll(0, 0);
                setTimeout(() => {
                    html.style.scrollBehavior = "smooth";
                })
            }
        });

        this._store.dispatch(new FetchStaff())
        this._store.dispatch(new FetchCategories())
    }

    public ngAfterViewInit() {
        this._matSideNav.openedChange.subscribe((opened) => {
            this._store.dispatch(new ToggleSideNav(opened))
        })
    }
}
