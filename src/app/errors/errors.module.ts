import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';
import { UnauthorizedPageComponent } from './components/unauthorized-page/unauthorized-page.component';

@NgModule({
  declarations: [NotFoundPageComponent, UnauthorizedPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
        { path: 'not-found', component: NotFoundPageComponent },
        { path: 'unauthorized', component: UnauthorizedPageComponent }
    ])
  ]
})
export class ErrorsModule { }
