import { Observable } from 'rxjs';
import { FeaturedState } from './../../../core/state/index';
import { FetchPageChildren } from './../../state/page.actions';
import { Store, Select } from '@ngxs/store';
import { WPPage } from './../../interfaces/wp-page.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'pulsar-page-star-citizen',
    templateUrl: './page-star-citizen.component.html',
    styleUrls: ['./page-star-citizen.component.scss']
})
export class PageStarCitizenComponent implements OnInit {

    @Input() public page: WPPage;

    @Select((state: FeaturedState) => state.page.children)
    public children$: Observable<WPPage[]>

    constructor(
        protected _store: Store
    ) {
    }
    
    ngOnInit() {
        this._store.dispatch(new FetchPageChildren(this.page.id));
    }

}
