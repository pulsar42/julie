import { WPApiService } from './../../core/services/wp-api.service';
import { WPPage } from './../interfaces/wp-page.interface';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { FetchPageList, FetchPageChildren } from './page.actions';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

export class PageStateModel {
    public pages: WPPage[];
    public children: WPPage[];
}

@State<PageStateModel>({
    name: 'page',
    defaults: {
        pages: [],
        children: []
    }
})
export class PageState {

    constructor(
        protected _wp: WPApiService,
    ) { }

    /**
     * Find a page by URL if exists
     * @param state 
     */
    @Selector()
    static pageByUrl(state: PageStateModel) {
        return (url: string) => {
            return state.pages.find((page) => page.link.replace(environment.wordpress.url.slice(0, environment.wordpress.url.length - 1), '').startsWith(url))
        };
    }

    /**
     * Fetch the list of all pages
     */
    @Action(FetchPageList)
    fetchPageList(ctx: StateContext<PageStateModel>, action: FetchPageList) {
        return this._wp.get<WPPage[]>(`wp/v2/pages/?slug=${action.slug}`).pipe(
            map(pages => { return ctx.patchState({ pages }) })
        )
    }

    @Action(FetchPageChildren)
    fetchPageChildren(ctx: StateContext<PageStateModel>, action: FetchPageChildren) {
        ctx.patchState({ children: [] });

        return this._wp.get<WPPage[]>(`pulsar42/meta/page_subs/${action.pageId}`).pipe(
            map(pages => { return ctx.patchState({ children: pages }) })
        )
    }

}
