export class PageAction {
    static readonly type = '[Page] Add item';
    constructor(public payload: string) { }
}

export class FetchPageList {
    static readonly type = '[Page] Fetch page list';
    constructor(public slug: string) { }
}

export class FetchPageChildren {
    static readonly type = '[Page] Fetch page children';
    constructor(public pageId: number) { }
}