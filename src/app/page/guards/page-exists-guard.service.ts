import { FeaturedState } from './../../core/state/index';
import { Store } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { FetchPageList } from '../state/page.actions';
import { map, switchMap, take, tap } from 'rxjs/operators';
import { PageState } from '../state/page.state';
import { of } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PageExistsGuardService implements CanActivate {
    constructor(
        protected _store: Store,
        protected _router: Router
    ) { }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const slugs = state.url.split('/');
        const slug = slugs[slugs.length - 1];

        return this._store.select(PageState.pageByUrl).pipe(
            map(selectPageByUrl => selectPageByUrl(state.url)),
            take(1),
            switchMap((page) => {
                if (page) {
                    return of(true);
                } else {
                    return this._store.dispatch(new FetchPageList(slug)).pipe(
                        switchMap((newState) => {
                            return this._store.select(PageState.pageByUrl).pipe(
                                map(selectPageByUrl => selectPageByUrl(state.url)),
                                take(1),
                                map(page => Boolean(page)),
                                tap((shouldNavigate) => {
                                    if (!shouldNavigate) this._router.navigateByUrl('/error/not-found')
                                })
                            )
                        })
                    )
                }
            })
        )
    }

}
