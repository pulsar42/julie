import { CoreModule } from './../core/core.module';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CommonModule } from '@angular/common';
import { PageComponent } from './components/page/page.component';
import { RouterModule } from '@angular/router';
import { PageState } from './state/page.state';
import { NgxsModule } from '@ngxs/store';
import { PageHeaderComponent } from './components/page/page-header/page-header.component';
import { PageStarCitizenComponent } from './templates/page-star-citizen/page-star-citizen.component';
import { PageCommunityComponent } from './templates/page-community/page-community.component';
import { MatGridListModule } from '@angular/material';
import { PageChangelogComponent } from './templates/page-changelog/page-changelog.component';
import { MarkdownModule } from 'ngx-markdown';

@NgModule({
    declarations: [PageComponent, PageHeaderComponent, PageStarCitizenComponent, PageCommunityComponent, PageChangelogComponent],
    imports: [
        CommonModule,
        CoreModule,
        MatGridListModule,
        NgxsModule.forFeature([PageState]),
        FlexLayoutModule,
        RouterModule.forChild([
            { path: '', component: PageComponent, pathMatch: 'full' }
        ]),
        MarkdownModule.forChild()
    ],
})
export class PageModule { }
