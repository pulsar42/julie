import { WPPage } from './../../../interfaces/wp-page.interface';
import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'pulsar-page-header',
    templateUrl: './page-header.component.html',
    styleUrls: ['./page-header.component.scss']
})
export class PageHeaderComponent implements OnInit {

    @Input() page: WPPage | null;

    constructor() { }

    ngOnInit() {
    }

}
