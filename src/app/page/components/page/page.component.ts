import { MetaService } from '@ngx-meta/core';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { WPPage } from '../../interfaces/wp-page.interface';
import { Observable } from 'rxjs';
import { Store } from '@ngxs/store';
import { PageState } from '../../state/page.state';
import { map } from 'rxjs/operators';

@Component({
    selector: 'pulsar-page',
    templateUrl: './page.component.html',
    styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

    /** current page */
    public page$: Observable<WPPage>;

    constructor(
        protected _store: Store,
        protected _route: Router,
        protected _meta: MetaService
    ) {
        this._route.events.subscribe(
            () => {
                this.page$ = this._store.select(PageState.pageByUrl).pipe(
                    map(selectPageByUrl => selectPageByUrl(_route.routerState.snapshot.url))
                );
            }
        )

    }

    ngOnInit() {
        this.page$.subscribe((page)  => {
            if(!page) return;
            const title = page.title.rendered.replace(/<[^>]*>?/gm, '');
            const excerpt = page.uagb_excerpt.replace(/<[^>]*>?/gm, '');

            this._meta.setTitle(`Pulsar42 - ${title}`, true);
            this._meta.setTag('og:title', `Pulsar42 - ${title}`);
            this._meta.setTag('og:description', excerpt);
            this._meta.setTag('og:image', page.uagb_featured_image_src.full[0]);
        })
    }

}
