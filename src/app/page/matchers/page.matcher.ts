import { WPPage } from './../interfaces/wp-page.interface';
import { UrlSegment, UrlMatchResult, UrlSegmentGroup, Route } from '@angular/router';
import { environment } from '../../../environments/environment';

/**
 * URL Matcher for our pages
 * matches `:slug[/:slug]`
 *
 * ex: `a-page`
 * `parent-page/child-page`
 */
export function pageMatcher(segments: UrlSegment[], group: UrlSegmentGroup, route: Route): UrlMatchResult {
    // Do **NOT** match empty urls
    if (!segments.length) return null;
    if (segments.length === 1 && (segments[0].path === "" || segments[0].path === "/")) return null;


    return { consumed: segments, posParams: { slug: segments[segments.length - 1] } };
};

/**
 * Generate the navigation link for a page
 */
export function generateLinkForPage(article: WPPage): string[] {
    return ["/", ...article.link.replace(environment.wordpress.url, "").split('/').filter((str) => Boolean(str))];
}