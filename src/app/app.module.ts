import { CoreState } from './core/state/core.state';
import { CoreModule } from './core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { NgxsModule } from '@ngxs/store';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsStoragePluginModule } from '@ngxs/storage-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { environment } from '../environments/environment';
import { singleArticleMatcher } from './single/matchers/article.matcher';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { pageMatcher } from './page/matchers/page.matcher';
import { PageExistsGuardService } from './page/guards/page-exists-guard.service';
import { MetaModule } from '@ngx-meta/core';
import { MarkdownModule } from 'ngx-markdown';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule.withServerTransition({ appId: 'pulsar-julie' }),
        RouterModule.forRoot([
            {
                path: 'home',
                loadChildren: './home/home.module#HomeModule'
            },
            {
                // path: ':year/:month/:day/:slug',
                matcher: singleArticleMatcher,
                loadChildren: './single/single.module#SingleModule'
            },
            {
                path: 'preview/:id',
                loadChildren: './single/single.module#SingleModule',
            },
            {
                path: 'events',
                loadChildren: './events/events.module#EventsModule'
            },
            {
                path: 'archive',
                loadChildren: './archive/archive.module#ArchiveModule'
            },
            {
                path: 'search',
                loadChildren: './single-search/single-search.module#SingleSearchModule'
            },
            { 
                path: 'error', 
                loadChildren: './errors/errors.module#ErrorsModule'
            },
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'home',
            },
            {
                matcher: pageMatcher,
                canActivate: [PageExistsGuardService],
                loadChildren: './page/page.module#PageModule'
            },
        ]),
        HttpClientModule,
        // TransferHttpCacheModule,
        BrowserAnimationsModule,
        NgxsModule.forRoot([CoreState], { developmentMode: !(environment.production && !environment.beta) }),
        NgxsRouterPluginModule.forRoot(),
        NgxsFormPluginModule.forRoot(),
        NgxsStoragePluginModule.forRoot({ key: ["core"] }),
        NgxsReduxDevtoolsPluginModule.forRoot({ disabled: environment.production && !environment.beta }),
        NgxsLoggerPluginModule.forRoot({ disabled: environment.production && !environment.beta }),
        CoreModule.forRoot(),
        MetaModule.forRoot(),
        MarkdownModule.forRoot({ loader: HttpClient })
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
