import { FeaturedState } from './../../../core/state/index';
import { FetchArchive, FetchCategory } from './../../state/archive.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { APISingleArticle } from '../../../single/interfaces/articles.interface';
import { Observable } from 'rxjs';
import { archiveCategoryLinkGenerator } from '../../matchers/archive.matcher';

@Component({
    selector: 'pulsar-article-archive',
    templateUrl: './article-archive.component.html',
    styleUrls: ['./article-archive.component.scss']
})
export class ArticleArchiveComponent implements OnInit {

    /** articles for the current category & page */
    @Select((state: FeaturedState) => state.archive.articles)
    public articles$: Observable<APISingleArticle[]>;

    /** how many articles fit our criterias */
    @Select((state: FeaturedState) => state.archive.itemsNumber)
    public itemsNumber$: Observable<number>;

    public pageNumber: number;

    constructor(
        protected _activatedRoute: ActivatedRoute,
        protected _store: Store,
        protected _router: Router,
    ) {
        this._activatedRoute.params.subscribe((params) => {
            this._store.dispatch(new FetchCategory(params.catId))
            this._store.dispatch(new FetchArchive(params.catId, params.pageNumber ? params.pageNumber : 1))

            this.pageNumber = params.pageNumber;
        });
    }

    ngOnInit() {
    }

    public pageChange(page: number) {
        let route = ['/archive/'];
        const archive = this._store.selectSnapshot((state: FeaturedState) => state.archive.archive);

        if(archive) {
            route = archiveCategoryLinkGenerator(archive);
        }

        this._router.navigate([...route, page]);
    }

}
