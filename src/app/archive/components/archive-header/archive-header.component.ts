import { MetaService } from '@ngx-meta/core';
import { APICategory } from './../../interfaces/api-category.interface';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { FeaturedState } from '../../../core/state';
import { Observable } from 'rxjs';
import { APISingleArticle } from '../../../single/interfaces/articles.interface';

@Component({
    selector: 'pulsar-archive-header',
    templateUrl: './archive-header.component.html',
    styleUrls: ['./archive-header.component.scss']
})
export class ArchiveHeaderComponent implements OnInit {

    /** featurette articles for the current category  */
    @Select((state: FeaturedState) => state.archive.featuredArticles)
    public featurettes$: Observable<APISingleArticle[]>;
    

    /** current category */
    @Select((state: FeaturedState) => state.archive.archive)
    public category$: Observable<APICategory>;
    

    constructor(
        protected _activatedRoute: ActivatedRoute,
        protected _meta: MetaService,
    ) { }

    ngOnInit() {
        this.category$.subscribe((category)  => {
            if(!category) return;
            const title = category.name;
            const excerpt = category.description;

            this._meta.setTitle(`Pulsar42 - Archive ${title}`, true);
            this._meta.setTag('og:title', `Pulsar42 - Archive ${title}`);
            this._meta.setTag('og:description', excerpt);
        })
    }

}
