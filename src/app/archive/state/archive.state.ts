import { WPApiService } from './../../core/services/wp-api.service';
import { State, Action, StateContext, Selector } from '@ngxs/store';
import { ArchiveAction, FetchArchive, FetchFeaturetteArchive, FetchCategory } from './archive.actions';
import { APISingleArticle } from '../../single/interfaces/articles.interface';
import { tap, map } from 'rxjs/operators';
import { APICategory } from '../interfaces/api-category.interface';

export class ArchiveStateModel {
    currentArchive: number | null;
    articles: APISingleArticle[] | null;
    featuredArticles: APISingleArticle[] | null;
    archive: APICategory | null;
    itemsNumber: number;
}

@State<ArchiveStateModel>({
    name: 'archive',
    defaults: {
        currentArchive: null,
        articles: null,
        featuredArticles: null,
        archive: null,
        itemsNumber: 0
    }
})
export class ArchiveState {
    constructor(
        protected _wp: WPApiService
    ) { }

    @Action(FetchArchive)
    fetchArchive(ctx: StateContext<ArchiveStateModel>, action: FetchArchive) {
        if (action.catId != ctx.getState().currentArchive || (action.catId === undefined && !ctx.getState().featuredArticles) ) {
            ctx.patchState({ articles: [] });
            ctx.dispatch(new FetchFeaturetteArchive(action.catId));
        }



        return this._fetchArchive(action.catId, false, action.pageNumber).pipe(map((res) => {
            return ctx.patchState({ articles: res.body, currentArchive: action.catId, itemsNumber: Number(res.headers.get('X-WP-Total')) })
        }));
    }

    @Action(FetchFeaturetteArchive)
    fetchFeaturetteArchive(ctx: StateContext<ArchiveStateModel>, action: FetchFeaturetteArchive) {
        if (Number(action.catId) !== Number(ctx.getState().currentArchive)) {
            ctx.patchState({ featuredArticles: [] });
        }

        return this._fetchArchive(action.catId, true).pipe(map((res) => {
            return ctx.patchState({ featuredArticles: res.body })
        }));
    }

    @Action(FetchCategory)
    fetchCategory(ctx: StateContext<ArchiveStateModel>, action: FetchCategory) {
        if (Number(action.catId) !== Number(ctx.getState().currentArchive)) {
            ctx.patchState({ archive: null });
        }

        if(action.catId) {
            return this._wp.get(`wp/v2/categories/${action.catId}`).pipe(map((archive) => {
                return ctx.patchState({ archive })
            }));
        }
    }


    protected _fetchArchive(catId: number, featuretted: boolean = false, page?: number) {
        const args = {
            per_page: featuretted ? "3" : "12",
            "type[]": ['post', 'traduction']
        };

        if (featuretted) {
            args["tags[]"] = ["10", "11"];
        } else {
            if(page) {
                args['page'] = page;
            }
        }

        if (catId) {
            args["categories[]"] = String(catId);
        }



        return this._wp.get<APISingleArticle[]>(`wp/v2/multiple-post-type`, args, "response");
    }

}
