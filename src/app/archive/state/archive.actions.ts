export class ArchiveAction {
    static readonly type = '[Archive] Add item';
    constructor(public payload: string) { }
}


/**
 * Fetch in the archive
 */
export class FetchArchive {
    static readonly type = "[ARCHIVE] Fetch Archive";
    constructor(
        public catId: number,
        public pageNumber: number
    ) { }
}

/**
 * Fetch featurette articles for this archive
 */
export class FetchFeaturetteArchive {
    static readonly type = "[ARCHIVE] Fetch Featurette Archive";
    constructor(
        public catId: number,
    ) { }
}

export class FetchCategory {
    static readonly type = "[ARCHIVE] Fetch Category";
    constructor(
        public catId: number
    ) { }
}

