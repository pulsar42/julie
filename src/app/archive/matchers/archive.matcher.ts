import { APICategory } from './../interfaces/api-category.interface';
import { UrlSegment, UrlMatchResult, UrlSegmentGroup, Route } from '@angular/router';

/**
 * URL Matcher for our categories archive
 * matches `/archive/[catId]-[catslug]/[:pageNumber]`
 *
 * ex: `a-page`
 * `parent-page/child-page`
 */
export function archiveMatcher(segments: UrlSegment[], group: UrlSegmentGroup, route: Route): UrlMatchResult {
    const idSlug = segments[0];
    if (!idSlug) return null;
    let pageNumber = 1;

    const m = idSlug.path.match(/^([0-9]+)-(.*)$/);
    if (!m || !m[1] || !m[2] || isNaN(Number(m[1]))) return null;

    if (segments[1] && !isNaN(Number(segments[1].path))) {
        pageNumber = Number(segments[1].path);
    }

    return {
        consumed: segments, posParams: {
            catId: new UrlSegment(m[1], {}),
            catSlug: new UrlSegment(m[2], {}),
            pageNumber: new UrlSegment(String(pageNumber), {})
        }
    };
};

/**
 * Generate the navigation link for a given category
 */
export function archiveCategoryLinkGenerator(category: APICategory | { id: number, slug: string }): string[] {
    
    const id = category.id ? category.id : (category as any).cat_ID;


    return [`/archive/${id}-${category.slug}`];
}