import { CoreModule } from './../core/core.module';
import { HomeArticlesComponent } from './components/home-articles/home-articles.component';
import { ArchiveState } from './state/archive.state';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleArchiveComponent } from './components/article-archive/article-archive.component';
import { EventArchiveComponent } from './components/event-archive/event-archive.component';
import { NgxsModule } from '@ngxs/store';
import { RouterModule } from '@angular/router';
import { ArchiveHeaderComponent } from './components/archive-header/archive-header.component';
import { archiveMatcher } from './matchers/archive.matcher';
import { NgxPaginationModule } from 'ngx-pagination'; // <-- import the module

@NgModule({
    declarations: [ArticleArchiveComponent, EventArchiveComponent, HomeArticlesComponent, ArchiveHeaderComponent],
    imports: [
        CommonModule,
        CoreModule,
        NgxsModule.forFeature([ArchiveState]),
        RouterModule.forChild([
            { matcher: archiveMatcher, component: ArticleArchiveComponent },
            { path: '', component: ArticleArchiveComponent, pathMatch: 'full' },
            { path: ':pageNumber', component: ArticleArchiveComponent },
        ]),
        NgxPaginationModule
    ]
})
export class ArchiveModule { }
