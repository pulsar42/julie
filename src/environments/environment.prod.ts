export const environment = {
    production: true,
    beta: false,
    hmr: false,
    wordpress: {
        url: `https://wp.pulsar42.sc/`
    },
    selfUrl: `https://pulsar42.sc/`
};
