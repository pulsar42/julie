export const environment = {
    production: true,
    beta: true,
    hmr: false,
    wordpress: {
        url: `https://wp.beta.pulsar42.sc/`
    },
    selfUrl: `https://beta.pulsar42.sc/`
};
