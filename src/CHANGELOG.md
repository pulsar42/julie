# [1.3.0](https://gitlab.com/pulsar42/julie/compare/1.2.2...1.3.0) (2019-07-21)


### Features

* ajout de la page changelog ([7937bfe](https://gitlab.com/pulsar42/julie/commit/7937bfe))


## [1.2.2](https://gitlab.com/pulsar42/julie/compare/1.2.1...1.2.2) (2019-07-21)


### Bug Fixes

* **rss:** fix liens des articles dans le flux rss ([c327781](https://gitlab.com/pulsar42/julie/commit/c327781))

## [1.2.1](https://gitlab.com/pulsar42/julie/compare/1.2.0...1.2.1) (2019-07-21)

### Style
* **single** correction de l'affichage des vignettes youtube dans un article

# [1.2.0](https://gitlab.com/pulsar42/julie/compare/1.1.0...1.2.0) (2019-07-21)


### Features

* **single:** meilleur gestion des articles custom RSI ([7dfef57](https://gitlab.com/pulsar42/julie/commit/7dfef57))

# [1.1.0](https://gitlab.com/pulsar42/julie/compare/1.0.0...1.1.0) (2019-07-07)


### Bug Fixes

* **event:** correction de l'affichage des evenements non bar-citizen sur la page d'index ([d54c82f](https://gitlab.com/pulsar42/julie/commit/d54c82f))


### Features

* nouveau thème sombre! ([070b489](https://gitlab.com/pulsar42/julie/commit/070b489))

# 1.0.0 (2019-06-27)


### Bug Fixes

* aot build ([cbdbeb8](https://gitlab.com/pulsar42/julie/commit/cbdbeb8))
* **ga:** add pageviews ([0d7bf96](https://gitlab.com/pulsar42/julie/commit/0d7bf96))
* **header:** menu not displayed because map vs array ([564c77d](https://gitlab.com/pulsar42/julie/commit/564c77d))
* **page:** fix page meta ([63b4be2](https://gitlab.com/pulsar42/julie/commit/63b4be2))
* **twitch:** banner not disapearing ([3cd1941](https://gitlab.com/pulsar42/julie/commit/3cd1941))
* **yt:** disable pointer-events ([7a5ebb8](https://gitlab.com/pulsar42/julie/commit/7a5ebb8))
* fix rss in prod ([7b2aae5](https://gitlab.com/pulsar42/julie/commit/7b2aae5))
* remove transferstate to bust cache ([04e171f](https://gitlab.com/pulsar42/julie/commit/04e171f))


### Features

* analytics ([70f8fff](https://gitlab.com/pulsar42/julie/commit/70f8fff))
* archive + pagination ([cc652c6](https://gitlab.com/pulsar42/julie/commit/cc652c6))
* dark mode ([a45624b](https://gitlab.com/pulsar42/julie/commit/a45624b))
* favicon + sem release ([70240a9](https://gitlab.com/pulsar42/julie/commit/70240a9))
* footer ([95eaa3d](https://gitlab.com/pulsar42/julie/commit/95eaa3d))
* header & footer ([492a0e9](https://gitlab.com/pulsar42/julie/commit/492a0e9))
* link to single ([c312239](https://gitlab.com/pulsar42/julie/commit/c312239))
* seo & meta ([5efd46d](https://gitlab.com/pulsar42/julie/commit/5efd46d))
* **page:** pages are now supported ([3cd73e4](https://gitlab.com/pulsar42/julie/commit/3cd73e4))
* tease ok ([b4d3422](https://gitlab.com/pulsar42/julie/commit/b4d3422))
* **article:** support full custom articles ([05a0694](https://gitlab.com/pulsar42/julie/commit/05a0694))
* **header:** dynamic header ([444552f](https://gitlab.com/pulsar42/julie/commit/444552f))
* **home:** event cards ([5368bfa](https://gitlab.com/pulsar42/julie/commit/5368bfa))
* **home:** featured event ([3ced01d](https://gitlab.com/pulsar42/julie/commit/3ced01d))
* **page:** sc page ([3437ba5](https://gitlab.com/pulsar42/julie/commit/3437ba5))
* **page:** simple page ([3b75530](https://gitlab.com/pulsar42/julie/commit/3b75530))
* **rss:** serve rss ([a7e2d1d](https://gitlab.com/pulsar42/julie/commit/a7e2d1d))
* **single:** add single page ([1107be1](https://gitlab.com/pulsar42/julie/commit/1107be1))
* twitch banner when live ([7a46264](https://gitlab.com/pulsar42/julie/commit/7a46264))
* **single:** display categories ([d5aeeef](https://gitlab.com/pulsar42/julie/commit/d5aeeef))
* **wp:** first release of wordpress formating ([1dc177c](https://gitlab.com/pulsar42/julie/commit/1dc177c))
* youtube easy embed ([e007010](https://gitlab.com/pulsar42/julie/commit/e007010))
