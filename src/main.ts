import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { BootstrapModuleFn as Bootstrap, hmr, WebpackModule } from '@ngxs/hmr-plugin';

declare const module: WebpackModule;

if (environment.production) {
    enableProdMode();
}

const bootstrap: Bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule);

if (environment.hmr) {
    hmr(module, bootstrap).catch(err => console.error(err));
} else {
    document.addEventListener('DOMContentLoaded', () => {
        bootstrap().catch(err => console.log(err));
    });
}
